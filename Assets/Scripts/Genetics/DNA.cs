﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNA {

    public List<Vector2> genomes = new List<Vector2>();

    public DNA(int genomeCount)
    {
        for (int i = 0; i < genomeCount; i++)
        {
            genomes.Add(new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)));
        }
    }
    public DNA(DNA dnaToCopy)
    {
        for (int i = 0; i < dnaToCopy.genomes.Count; i++)
        {
            genomes.Add(dnaToCopy.genomes[i]);
        }
    }

    public DNA(DNA parent, DNA partner , float mutationRate = 0.01f)
    {
        for (int i = 0; i < parent.genomes.Count; i++)
        {
            float mutationChance = Random.Range(0, 1.0f);

            if (mutationChance <= mutationRate)
                genomes.Add(MutateGenome());
            else
            {
                int random = Random.Range(0, 2);

                if(random == 0)
                    genomes.Add(parent.genomes[i]);
                else
                    genomes.Add(partner.genomes[i]);
            }
        }
    }

    private Vector2 MutateGenome()
    {
        return new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
    }
}
