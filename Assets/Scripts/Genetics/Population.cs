﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Population : MonoBehaviour {

    [HideInInspector]
    public List<Creature> population = new List<Creature>();

    [Header("Creature")]
    public GameObject creaturePrefab;
    [Range(1, 50)]
    public float creatureSpeed;

    [Header("Genetics"), Range(0, 3)]
    public int eliteGroup = 1;
    [Range(50, 200)]
    public int populationSize = 100;
    [Range(1.5f, 3.0f)]
    public float genomMultiplier = 2.0f;
    [Range(3, 20)]
    public float survivorPercentage = 5;
    [Range(0.005f, 0.05f)]
    public float mutationRate = 0.01f;

    internal int BestPossibleScore;
    internal int GenomeCount;
    internal int PreviousFinishers;
    internal float Record;
    internal bool isActive;
    internal Track track;

    internal int Generation
    {
        get
        {
            return _Generation;
        }
        private set
        {
            _Generation = value;
            isActive = true;
            AliveMembers = populationSize;
            if (Generation > 1)
                Record = GetFittest(1)[0].GetFitness();
            else
                Record = 0;
            PreviousFinishers = Finishers;
            Finishers = 0;
        }
    }
    private int _Generation;
    private int AliveMembers { get { return _AliveMembers; } set { _AliveMembers = value; } }
    private int _AliveMembers;
    private int Finishers { get; set; }

    public void Init(Track track)
    {
        this.track = track;
        Generation = 0;

        ClearAndDestoyPopulation();
        InitPopulation();
    }

    public void Hide()
    {
        for (int i = 0; i < population.Count; i++)
        {
            population[i].gameObject.SetActive(false);
        }
    }

    public void Terminate()
    {
        isActive = false;
        ClearAndDestoyPopulation();
    }

    private void Update()
    {
        if (isActive && !MemebersAlive())
        {
            if(!FindObjectOfType<MainController>().GenerationFinished(this))
                NextGeneration();
            else
            {
                isActive = false;
                Hide();
            }
        }
    }

    private void InitPopulation()
    {
        Generation++;

        Vector2[] wayPointPositions = TransformsToVectorTwos(track.wayPoints);


        for (int i = 0; i < populationSize; i++)
        {
            GameObject creature = Instantiate(creaturePrefab, track.start.position, Quaternion.identity);
            population.Add(creature.GetComponent<Creature>());
            population[i].InitCreature(this, new DNA(CalculateGenomeSize()), track.target.position, creatureSpeed, wayPointPositions);
            population[i].transform.SetParent(transform);
        }

    }

    private void NextGeneration()
    {
        Generation++;

        int survivorCount = Mathf.RoundToInt(populationSize * (survivorPercentage * 0.01f));
        List<Creature> survivors = GetFittest(survivorCount);

        Vector2[] wayPointPositions = TransformsToVectorTwos(track.wayPoints);

        ClearAndDestoyPopulation();

        for (int i = 0; i < eliteGroup; i++)
        {
            GameObject creature = Instantiate(creaturePrefab, track.start.position, Quaternion.identity);
            creature.GetComponent<Creature>().InitCreature(this, new DNA(survivors[i].dna), track.target.position, creatureSpeed, wayPointPositions);
            creature.transform.SetParent(transform);
            population.Add(creature.GetComponent<Creature>());
        }

        while (population.Count < populationSize)
        {
            for (int i = 0; i < survivors.Count; i++)
            {
                GameObject creature = Instantiate(creaturePrefab, track.start.position, Quaternion.identity);
                DNA creaturesDNA = new DNA(survivors[i].dna, survivors[Random.Range(0, survivors.Count)].dna, mutationRate);
                creature.GetComponent<Creature>().InitCreature(this, creaturesDNA, track.target.position, creatureSpeed, wayPointPositions);
                creature.transform.SetParent(transform);
                population.Add(creature.GetComponent<Creature>());
            }
        }

        for (int i = 0; i < survivors.Count; i++)
        {
            Destroy(survivors[i].gameObject);
        }       
    }
    private int CalculateGenomeSize()
    {
        float genomeSize = 0;
        for (int i = 0; i <= track.wayPoints.Length; i++)
        {
            if (track.wayPoints.Length > 0)
            {
                if (i == 0)
                    genomeSize += Vector2.Distance(track.start.position, track.wayPoints[i].position);
                else if (i < track.wayPoints.Length)
                    genomeSize += Vector2.Distance(track.wayPoints[i - 1].position, track.wayPoints[i].position);
                else
                    genomeSize += Vector2.Distance(track.wayPoints[i - 1].position, track.target.position);
            }
            else
                genomeSize += Vector2.Distance(track.start.position, track.target.position);
        }

        GenomeCount = Mathf.CeilToInt(genomeSize);
        BestPossibleScore = GenomeCount + track.wayPoints.Length;
       
        genomeSize *= genomMultiplier;
        int genomeSizeasInt = Mathf.CeilToInt(genomeSize);

        return genomeSizeasInt;
    }

    private List<Creature> GetFittest(int count)
    {
        population.Sort(ByFittest);

        List<Creature> fittest = new List<Creature>();       

        for (int i = 0; i < count; i++)
        {
            if (i < population.Count)
                fittest.Add(population[i]);
            else
                break;
        }
        return fittest;
    }

    public int ByFittest(Creature a, Creature b)
    {
        if (a.GetFitness() > b.GetFitness())
            return -1;
        else if (a.GetFitness() < b.GetFitness())
            return 1;
        else
            return 0;

    }

    public Vector2[] TransformsToVectorTwos(Transform[] wayPoints)
    {
        Vector2[] wayPointsasVectors = new Vector2[wayPoints.Length];
        int i = 0;
        foreach (var transform in wayPoints)
        {
            wayPointsasVectors[i++] = transform.position;
        }
        return wayPointsasVectors;
    }

    internal void MemberFoundTarget()
    {
        Finishers++;
    }

    internal void MemberDead()
    {
        AliveMembers--;
    }

    private bool MemebersAlive()
    {
        return AliveMembers > 0;
    }

    private void ClearAndDestoyPopulation()
    {
        for (int i = 0; i < population.Count; i++)
        {
            Destroy(population[i].gameObject);
        }
        population.Clear();
    }
}
