﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour {

    internal DNA dna;
    internal bool isDead;

    private Population family;

    private float timeAlive;
    private float creatureSpeed;
    private int pathPointer;
    private int wayPointPointer;
    private bool wallWasHit;

    private Vector2 target;
    private Vector2 nextPoint;
    private Vector2[] wayPoints;

    public void InitCreature(Population family, DNA dna, Vector2 target, float creatureSpeed, Vector2[] wayPoints)
    {
        this.dna = dna;
        this.target = target;
        this.wayPoints = wayPoints;
        this.creatureSpeed = creatureSpeed;
        this.family = family;

        nextPoint = transform.position;
    }
    public float GetFitness()
    {
        float dist = 0;
        float bonus = 0;

        if (wayPoints.Length > 0 && wayPointPointer < wayPoints.Length)
            dist = Vector2.Distance(transform.position, wayPoints[wayPointPointer]);
        else
            dist = Vector2.Distance(transform.position, target);

        dist = 1 / dist;

        if (dist > 1.0f)
            dist = 1.0f;

        if (wayPointPointer == wayPoints.Length && Vector2.Distance(transform.position, target) < 1.0f)
        {
            bonus += (dna.genomes.Count - pathPointer);
            bonus += 1.0f / timeAlive;
        }


        if (wallWasHit)
        {
            bonus -= 0.5f;
            wallWasHit = false;
        }

        return dist + wayPointPointer + bonus;
    }

    private void FixedUpdate()
    {
        if (!isDead)
        {
            timeAlive += Time.deltaTime;

            if (wayPointPointer < wayPoints.Length && Vector2.Distance(transform.position, wayPoints[wayPointPointer]) < 1.0f)
                wayPointPointer++;
            else if (wayPointPointer == wayPoints.Length && Vector2.Distance(transform.position, target) < 1.0f)
                FoundTarget();

            if (pathPointer == dna.genomes.Count)
                Terminate();
            else if ((Vector2)transform.position == nextPoint)
                nextPoint = (Vector2)transform.position + dna.genomes[pathPointer++];
            else
                transform.position = Vector2.MoveTowards(transform.position, nextPoint, creatureSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Obstacle")
        {
            wallWasHit = true;
            Terminate();
        }
    }

    private void FoundTarget()
    {
        if(!isDead)
        {
            family.MemberFoundTarget();
            Terminate();
        }
    }

    private void Terminate()
    {
        if(!isDead)
        {
            isDead = true;
            family.MemberDead();
        }
    }
}
