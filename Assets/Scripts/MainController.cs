﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MainController : MonoBehaviour {

    [HideInInspector]
    public List<Population> populations;
    [HideInInspector]
    public List<StatisticsButton> statisticsButtons;

    [Header("Set up")]
    public TrackCreator trackCreator;
    public Transform tabs;
    public GameObject replayButton;
    public GameObject stopButton;
    public GameObject populationPrefab;
    public GameObject statisticButtonPrefab;

    [Header("UI")]
    public Text generationText;
    public Text recordText;
    public Text finishersText;
    public Text genomesText;
    public Text bestPossibleScoreText;

    [Header("Goals")]
    [Range(50, 1000), Tooltip("Generation limit.")]
    public int maxGenerationCount = 50;
    [Range(1, 4), Tooltip("Number of populations at once")]
    public int populationCount = 1;

    private int shownStaticticsIndex;

    private List<Creature> bestIndividuals = new List<Creature>();

    public bool GenerationFinished(Population population)
    {
        for (int i = 0; i < populations.Count; i++)
        {
            if (populations[i] == population && shownStaticticsIndex == i)
                ButtonClick_ShowStatistics(i);
        }

        if (population.Generation >= maxGenerationCount || population.Record >= population.BestPossibleScore)
        {
            population.population.Sort(population.ByFittest);
            bestIndividuals.Add(population.population[0]);

            int completedPopulations = 1;
            for (int i = 0; i < populations.Count; i++)
            {
                if (populations[i] != population && !populations[i].isActive)
                    completedPopulations++;
            }
            if ((completedPopulations >= populations.Count || population.Record >= population.BestPossibleScore))
            {
                replayButton.SetActive(true);
                stopButton.SetActive(false);

                for (int i = 0; i < populations.Count; i++)
                {
                    populations[i].Hide();
                }

                ButtonClick_ReplayBest();
            }
            return true;
        }
        return false;
    }


    public void ButtonClick_ReplayBest()
    {
        for (int i = 0; i < populations.Count; i++)
        {
            populations[i].population.Sort(populations[i].ByFittest);

            GameObject bestIndividual = Instantiate(populations[i].creaturePrefab, populations[i].track.start.position, Quaternion.identity);
            bestIndividual.GetComponent<Creature>().InitCreature(populations[i], populations[i].population[0].dna, populations[i].track.target.position, populations[i].creatureSpeed, populations[i].TransformsToVectorTwos(populations[i].track.wayPoints));
            bestIndividuals.Add(bestIndividual.GetComponent<Creature>());
        }
    }

    public void ButtonClick_Stop()
    {
        for (int i = 0; i < populations.Count; i++)
        {
            populations[i].Terminate();
        }       
    }

    public void ButtonClick_Start()
    {
        Track track = trackCreator.CreateNewTrack();
        ClearBests();
        ClearPopulations();
        ClearStatisticButtons();

        for (int i = 0; i < populationCount; i++)
        {
            GameObject population = Instantiate(populationPrefab, transform);
            population.name = "Population " + (i + 1);
            population.GetComponent<Population>().Init(track);
            populations.Add(population.GetComponent<Population>());

            GameObject button = Instantiate(statisticButtonPrefab, tabs);
            button.GetComponent<StatisticsButton>().Init(i);
            statisticsButtons.Add(button.GetComponent<StatisticsButton>());
        }

        ButtonClick_ShowStatistics(0);
    }

    public void ButtonClick_ShowStatistics(int index)
    {
        shownStaticticsIndex = index;

        for (int i = 0; i < statisticsButtons.Count; i++)
        {
            if (i == index)
                statisticsButtons[i].SetColour(Color.green);
            else
                statisticsButtons[i].SetColour(Color.white);
        }

        Population population = populations[index];

        generationText.text = population.Generation.ToString();
        recordText.text = population.Record.ToString("0.00");
        finishersText.text = population.PreviousFinishers.ToString();
        genomesText.text = population.GenomeCount.ToString();
        bestPossibleScoreText.text = population.BestPossibleScore.ToString();
}

    private void ClearBests()
    {
        for (int i = 0; i < bestIndividuals.Count; i++)
        {
            Destroy(bestIndividuals[i].gameObject);
        }
        bestIndividuals.Clear();
    }

    private void ClearStatisticButtons()
    {
        for (int i = 0; i < statisticsButtons.Count; i++)
        {
            Destroy(statisticsButtons[i].gameObject);
        }
        statisticsButtons.Clear();
    }

    private void ClearPopulations()
    {
        for (int i = 0; i < populations.Count; i++)
        {
            Destroy(populations[i].gameObject);
        }
        populations.Clear();
    }
}
