﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StatisticsButton : Selectable {

    private int index;

    public void SetColour(Color color)
    {
        GetComponent<Image>().color = color;
    }

    public void Init(int index)
    {
        this.index = index;
        GetComponentInChildren<Text>().text = "Population: " + (index + 1);
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        FindObjectOfType<MainController>().ButtonClick_ShowStatistics(index);
        base.OnPointerDown(eventData);
    }
}
