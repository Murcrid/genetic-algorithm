﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackCreator : MonoBehaviour
{
    [Header("Set up")]
    public GameObject obstaclePrefab;
    public GameObject startPrefab;
    public GameObject targetPrefab;
    public GameObject[] wayPointPrefabs;

    public Transform wayPointParent;
    public Transform obstacleParent;
    public Transform trackParent;

    [Header("Track size")]
    public float horizontalLength = 18.0f;
    public float verticalLength = 7.0f;
    public float horizontalOffset = 0.0f;
    public float verticalOffset = -1.0f;

    [Header("Track variables"), Range(1, 10)]
    public int obstacleDensity = 1;
    [Range(1.5f, 4.0f)]
    public float obstacleGap = 2;
    [Range(0.25f, 1.0f)]
    public float obstacleScaleMin = 1.0f;
    [Range(1.0f, 1.75f)]
    public float obstacleScaleMax = 1.0f;
    [Range(0, 5)]
    public int wayPointCount = 0;

    private List<GameObject> obstacles = new List<GameObject>();
    private Transform[] wayPoints = new Transform[0]; 
    private GameObject start, target;

    public Track CreateNewTrack()
    {
        ClearPreviousTrack();
        return CreateRandomObjects();
    }

    private void ClearPreviousTrack()
    {
        if(wayPoints != null)
        {
            for (int i = 0; i < wayPoints.Length; i++)
            {
                Destroy(wayPoints[i].gameObject);
            }
            wayPoints = new Transform[0];
        }
        if (obstacles != null)
        {
            for (int i = 0; i < obstacles.Count; i++)
            {
                Destroy(obstacles[i]);
            }
            obstacles.Clear();
            Destroy(start);
            Destroy(target);
        }
    }
    private Track CreateRandomObjects()
    {
        start = Instantiate(startPrefab,
                                       new Vector2(Random.Range(-(horizontalLength / 2) + 2 + horizontalOffset, (horizontalLength / 2) - 12 + horizontalOffset),
                                                   Random.Range(-(verticalLength / 2) + verticalOffset, (verticalLength / 2) + verticalOffset)),
                                       Quaternion.identity);
        start.transform.SetParent(trackParent);

        target = Instantiate(targetPrefab,
                                        new Vector2(Random.Range(-(horizontalLength / 2) + 12 + horizontalOffset, (horizontalLength / 2) - 2 + horizontalOffset),
                                                    Random.Range(-(verticalLength / 2) + verticalOffset, (verticalLength / 2) + verticalOffset)),
                                        Quaternion.identity);
        target.transform.SetParent(trackParent);

        int obstacleRarity = 1001 - obstacleDensity * 100;
        Debug.Log(obstacleRarity);


        for (float i = -(horizontalLength / 2) + horizontalOffset; i < (horizontalLength / 2) + horizontalOffset; i += 0.1f)
        {
            for (float x = -(verticalLength / 2) + verticalOffset; x < (verticalLength / 2) + verticalOffset; x += 0.1f)
            {               
                if (Random.Range(0, obstacleRarity) == 0)
                {
                    if (Physics2D.OverlapCircle(new Vector2(i, x), obstacleGap) == null)
                    {
                        GameObject obstacle = Instantiate(obstaclePrefab, new Vector2(i, x), Quaternion.identity);
                        obstacle.transform.Rotate(new Vector3(0, 0, Random.Range(0, 90)));
                        obstacle.transform.SetParent(obstacleParent);
                        float scaleSize = Random.Range(obstacleScaleMin, obstacleScaleMax);
                        obstacle.transform.localScale = new Vector3(scaleSize, scaleSize, 0);
                        obstacles.Add(obstacle);
                    }
                }               
            }
        }

        int placedWayPoints = 0;
        if(wayPointCount > 0)
        {
            wayPoints = new Transform[wayPointCount];

            while (placedWayPoints < wayPointCount)
            {
                float x = Mathf.Round(Random.Range(-(horizontalLength / 2) + horizontalOffset, (horizontalLength / 2) + horizontalOffset) * 100.0f) / 100.0f;
                float y = Mathf.Round(Random.Range(-(verticalLength / 2) + verticalOffset, (verticalLength / 2) + verticalOffset) * 100.0f) / 100.0f;

                Vector2 randomPos = new Vector2(x, y);

                if (Physics2D.OverlapCircle(randomPos, obstacleGap / 2) == null)
                {
                    Transform wayPoint = Instantiate(wayPointPrefabs[placedWayPoints], randomPos, Quaternion.identity).transform;
                    wayPoint.SetParent(obstacleParent);

                    wayPoints[placedWayPoints++] = wayPoint;
                }
            }
        }

        return new Track(start.transform, target.transform, wayPoints);
    }
}