﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track
{
    public Transform start;
    public Transform target;

    public Transform[] wayPoints;

    public Track(Transform start, Transform target, Transform[] wayPoints)
    {
        this.start = start;
        this.target = target;

        this.wayPoints = wayPoints;
    }
}
